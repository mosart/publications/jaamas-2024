import os
import platform
import subprocess

# global folders
OS = platform.system()
ARCH = platform.machine()
CASE_STUDIES_FOLDER = "./case_studies"
OUTPUT_FOLDER = "./output"
IMITATOR_PATH = f"./tools/imitator/imitator-{OS}-{ARCH}"
STCTL_PATH = "./tools/stctl"


# global parameters
METHODS = ["parameters", "variables"]
SYNTHESIS = ["one", "all"]


def create_folder(path):
    """Creates a folder if it does not exist

    Parameters
    ----------
    path : str
        Path of the new folder

    Examples
    --------

    >>> create_folder('./results')
    """
    if not os.path.exists(path):
        os.makedirs(path)


def get_name_from_path(path: str) -> str:
    """Get the name of a file from a path without it's extension

    Parameters
    ----------
    path : str
        Path of the file

    Returns
    -------
    str
    """
    name, _ = os.path.splitext(os.path.basename(path))
    return name


def run_command(command: str):
    """Run a command in the command line"""
    return subprocess.run(
        command.split(),
        capture_output=True,
        text=True,
        check=True,
    )


def run_imitator(
    model_file: str, formula_file: str, timeout: int, args: str = ""
) -> str:
    """Run imitator command

    Parameters
    ----------
    model_file : str
        Path to the model file
    formula_file : str
        Path to the formula file
    timeout : int
        Timeout in seconds
    args : str, optional
        extra arguments passed to the command, by default ""

    Returns
    -------
    str
        output of the process
    """
    formula_name = get_name_from_path(formula_file)
    model_name = get_name_from_path(model_file)

    # output prefix for imitator
    output_prefix = os.path.join(OUTPUT_FOLDER, formula_name, model_name)
    create_folder(os.path.dirname(output_prefix))

    default_args = f"-merge=none -time-limit {timeout} -output-prefix {output_prefix}"
    command = f"{IMITATOR_PATH} {default_args} {args} {model_file} {formula_file}"

    print(f"  > Running Imitator : {command}")
    if os.path.exists(f"{output_prefix}.res"):
        print("    skipped.")
        return ""

    output = run_command(command)
    return output.stdout


def generate_models(model: str, n: int, extra_args: str = "") -> str:
    """Generate models for the benchmarks

    Parameters
    ----------
    model : str
        name of the model (e.g. philo)
    n : int
        number of agents
    extra_args : str, optional
        extra arguments passed to the CLI, by default ""

    Returns
    -------
    str
        output of the CLI
    """
    command_path = os.path.join(STCTL_PATH, "generator.py")
    command = f"python3 {command_path} --n {n} --output_folder {CASE_STUDIES_FOLDER} {model} {extra_args}"

    print(f"  > Generating Model: {command}")
    output = run_command(command)

    return output.stdout


def generate_philo_model(n: int) -> str:
    """Generate the philo model

    Parameters
    ----------
    n : int
        number of philosophers

    Returns
    -------
    str
        output of the CLI
    """
    return generate_models("philo", n)


def generate_voter_model(n: int, c: int) -> str:
    """Generate the voters model

    Parameters
    ----------
    n : int
        number of voters
    c : int
        number of candidates

    Returns
    -------
    str
        output of the CLI
    """
    return generate_models("voter", n, f"--c {c}")


def generate_voter_formula(n: int, candidate: int = 1, coallision: int = 3):
    """Generate the formula for the voter model

    Parameters
    ----------
    n : int
        number of voters
    candidate : int
        candidate to be voted
    coallision : int, optional
        number of agents in the coallision, by default 3
    """
    agents_str = ",".join([f"voter{a}" for a in range(1, min(n, coallision) + 1)])
    voters_str = " ^ ".join([f"voter{v}.voted{v}{candidate}" for v in range(1, n + 1)])
    formula = f"<<{agents_str}>> EF [0,8] ({voters_str})"

    # save the formula
    filename = f"voter_n{n}-{coallision}.stctl"
    output_file = os.path.join(CASE_STUDIES_FOLDER, filename)
    print(f"  > Generating Formula : {output_file}")
    with open(output_file, "w") as f:
        f.write(formula)
        return output_file


def generate_philo_formula(n: int):
    """Generate the formula for the philo model

    Parameters
    ----------
    n : int
        number of philosophers
    """
    philo_str = " ^ ".join(
        [f"philo{p}.eating{p}" for p in range(0, n - 1) if p % 2 == 0]
    )
    formula = f"<<lackey>> EF [1,5) ({philo_str})"

    # save the formula
    filename = f"philo_n{n}.stctl"
    output_file = os.path.join(CASE_STUDIES_FOLDER, filename)
    print(f"  > Generating Formula : {output_file}")
    with open(output_file, "w") as f:
        f.write(formula)
        return output_file


def apply_transformation(
    model_file: str, property_file: str, method: str, synthesis: str
) -> str:
    """Apply the transformation to a specific model and property

    Parameters
    ----------
    model_file : str
        path to the model file
    property_file : str
        path to the property file
    method : str
        transformation method (parameters, variables)
    synthesis : str
        synthesis method (one, all)

    Returns
    -------
    str
        output of the CLI
    """
    command_path = os.path.join(STCTL_PATH, "app.py")
    model_name = get_name_from_path(model_file)
    output_filename = os.path.join(CASE_STUDIES_FOLDER, f"{model_name}_{method}.imi")
    command = f"python3 {command_path} --model {model_file} --property {property_file} --method {method} --synthesis {synthesis} --output {output_filename}"

    print(f"  > Running transformation: {command}")
    output = run_command(command)
    return output.stdout


def run_benchmark(model_file: str, property_file: str, timeout: int):
    """Run the benchmark of a model

    Parameters
    ----------
    model_file : str
        Path to the model
    property_file : str
        Path to the formula
    timeout : int
        timeout in seconds
    """
    for m in METHODS:
        for s in SYNTHESIS:
            print(f"\n  ++ Method: {m}, Synthesis: {s}")
            output = apply_transformation(model_file, property_file, m, s)
            _, model_instance_path, property_instance_path = (
                output.strip().replace("  - ", "").split("\n")
            )
            imitator_args = (
                "-states-description" if "variables" in model_instance_path else ""
            )
            run_imitator(
                model_instance_path,
                property_instance_path,
                timeout,
                imitator_args,
            )


def run_voters_benchmarks(
    max_voters: int, max_candidates: int, max_coallision: int, timeout: int
):
    """Run the benchmarks of the voters model

    Parameters
    ----------
    max_voters : int
        maximum number of voters
    max_candidates : int
        maximum number of candidates
    max_coallision : int
        maximum number of voters in the coallision
    timeout : int
        timeout in seconds
    """
    for n in range(1, max_voters + 1):
        for c in range(1, max_candidates + 1):
            for a in range(1, min(max_voters, max_coallision) + 1):
                print(
                    f"\n** Benchmark [voters]: {n} voters, {c} candidates, {a} voters in coallision"
                )
                # generate the formulas
                property_file = generate_voter_formula(n, 1, a)

                # generate the models
                output_model = generate_voter_model(n, c)
                model_filename = output_model.strip().split(" ")[2]

                print("  --  ")

                # run the benchmark
                run_benchmark(model_filename, property_file, timeout)


def run_philo_benchmarks(max_philosophers: int, timeout: int):
    """Run the benchmarks of the philosophers model

    Parameters
    ----------
    max_philosophers : int
        maximum number of philosophers
    timeout : int
        timeout in seconds
    """
    for n in range(2, max_philosophers + 1):
        print(f"\n** Benchmark [philo]: {n} philosophers")
        # generate the formulas
        property_file = generate_philo_formula(n)

        # generate the models
        output_model = generate_philo_model(n)
        model_filename = output_model.strip().split(" ")[2]

        print("  --  ")

        # run the benchmark
        run_benchmark(model_filename, property_file, timeout)


# create output folders
create_folder(CASE_STUDIES_FOLDER)
create_folder(OUTPUT_FOLDER)

# run benchmarks
run_voters_benchmarks(max_voters=15, max_candidates=4, max_coallision=3, timeout=120)
run_philo_benchmarks(max_philosophers=15, timeout=120)
