|                  |   ('all', 'parameters') |   ('all', 'variables') |   ('one', 'parameters') |   ('one', 'variables') |
|:-----------------|------------------------:|-----------------------:|------------------------:|-----------------------:|
| ('philo', 2, 1)  |                   0.005 |                  0.004 |                   0.003 |                  0.002 |
| ('philo', 3, 1)  |                   0.256 |                  0.051 |                   0.02  |                  0.019 |
| ('philo', 4, 1)  |                  95.675 |                 26.742 |                  14.477 |                 11.117 |
| ('philo', 5, 1)  |                 171.06  |                394.825 |                 171.19  |                165.221 |
| ('philo', 6, 1)  |                 369.317 |                296.703 |                 369.544 |                296.788 |
| ('philo', 7, 1)  |                 325.723 |                334.954 |                 325.89  |                333.814 |
| ('philo', 8, 1)  |                 174.89  |                222.003 |                 175.633 |                221.105 |
| ('philo', 9, 1)  |                 nan     |                nan     |                 nan     |                nan     |
| ('philo', 10, 1) |                 nan     |                nan     |                 nan     |                nan     |
| ('philo', 11, 1) |                 265.319 |                282.055 |                 265.673 |                282.313 |
| ('philo', 12, 1) |                 180.973 |                178.513 |                 180.508 |                178.443 |
| ('philo', 13, 1) |                 nan     |                nan     |                 nan     |                nan     |