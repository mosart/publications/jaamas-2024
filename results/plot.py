#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import plotly.express as px
import plotly.io as pio
import numpy as np
import os
import csv
import re

pio.kaleido.scope.mathjax = None
pio.kaleido.scope.default_width = 1800

OUTPUT_FOLDER = os.path.abspath("../output")
RESULTS_FOLDER = os.path.abspath(os.path.curdir)
STRATEGIES = ["one", "all"]
METHODS = ["parameters", "variables"]
MODELS = ["voter", "philo"]
TIMEOUT = 420  # timeout ( 7 minutes )


# In[2]:


def create_folder(path):
    """Creates a folder if it does not exist"""
    if not os.path.exists(path):
        os.makedirs(path)
    return path


# In[3]:


def generate_csv(model):
    """Reads the imitator logs and generates a csv file gathering the results"""
    regex = r"Total computation time\s+:\s+(\d+.?\d+)\s+second"
    folders = sorted(os.listdir(OUTPUT_FOLDER))

    # csv header
    fieldnames = ["model", "agents", "coallision", "synthesis", "method", "time"]
    if model == "voter":
        fieldnames.insert(2, "candidates")

    # create folder and csv file

    output_file = os.path.join(
        create_folder(os.path.join(RESULTS_FOLDER, model)), f"{model}.csv"
    )
    with open(output_file, "w") as f:
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        writer.writeheader()

        # Get instances of benchmarks
        for folder in folders:
            # skip folder not related with the model
            if not folder.startswith(model):
                continue

            model, configuration, synthesis = folder.split("_")
            agents = configuration[1:]
            coallision = "1"
            if "-" in agents:
                agents, coallision = agents.split("-")

            # list imitator output files
            files_folder = os.path.join(OUTPUT_FOLDER, folder)
            files = sorted(os.listdir(files_folder))
            for file in files:
                # ignore other files
                if not file.endswith(".res"):
                    continue

                # read imitator output file
                with open(os.path.join(files_folder, file), "r") as fin:
                    # get time (in seconds)
                    match = re.search(regex, fin.read())
                    time = "" if match is None else match.group(1)

                    # get the method used (parameters, variables)
                    info = file.replace(".res", "").split("_")
                    method = info[-1]

                    data = {
                        "model": model,
                        "agents": agents,
                        "coallision": coallision,
                        "synthesis": synthesis,
                        "method": method,
                        "time": time,
                    }

                    if model == "voter":
                        candidates = info[-2]
                        data["candidates"] = candidates

                    # write data
                    writer.writerow(data)


# In[4]:


def read_csv(filename):
    """returns a pandas dataframe from a csv file"""
    df = pd.read_csv(filename, sep=",", na_values="TO")
    df.loc[df["time"] > TIMEOUT, "time"] = np.NaN
    return df


# In[5]:


def plot(df, title):
    plot_args = {}
    if "candidates" in df.columns:
        plot_args = dict(
            color="candidates",
            facet_col="coallision",
            labels={
                "time": "time (s)",
                "agents": "# voters",
                "coallision": "# agents",
                "candidates": "# candidates",
            },
        )
    else:
        plot_args = dict(
            color="method",
            labels={
                "time": "time (s)",
                "agents": "# philosophers",
                "method": "method",
            },
        )

    # figure
    fig = px.bar(df, x="agents", y="time", barmode="group", title=title, **plot_args)
    for trace in fig["data"]:
        trace["name"] = trace["name"].replace("c", "")

    # add vertical lines separating groups
    indexes = sorted(list(set(sum([[i - 0.5, i + 0.5] for i in fig.data[0].x], []))))
    for i in indexes[1:-1]:
        fig.add_vline(x=i, line_width=1, line_dash="dash", line_color="gray")

    # title layout
    fig.update_layout(
        title={"y": 0.9, "x": 0.5, "xanchor": "center", "yanchor": "top"},
        margin=dict(t=200),
    )

    # figure layout
    nb_plots = len(df.coallision.unique())
    fig.update_layout(
        width=900 * nb_plots,
        height=900,
        plot_bgcolor="white",
        paper_bgcolor="white",
        bargap=0.2,
        font=dict(size=28),
    )

    # bars layout
    fig.update_traces(marker={"line": {"width": 1, "color": "rgb(0,0,0)"}})

    # legend layout
    fig.update_layout(
        legend=dict(
            orientation="h",
            bordercolor="grey",
            borderwidth=1,
            xanchor="center",
            x=0.5,
            y=-0.2,
        )
    )

    fig.update_xaxes(
        showline=True,
        linewidth=1,
        linecolor="black",
        mirror=True,
        dtick=1,
        matches=None,
    )
    fig.update_yaxes(
        showline=True,
        linewidth=1,
        linecolor="black",
        mirror=True,
        dtick=40,
        showgrid=True,
        gridwidth=1,
        gridcolor="LightGrey",
        griddash="dash",
        range=(0, TIMEOUT),
    )

    return fig


# In[6]:


def save_plot(fig, model, name):
    """Save a figure in png, pdf and html format"""
    output_file = os.path.join(create_folder(os.path.join(RESULTS_FOLDER, model)), name)
    fig.write_image(f"{output_file}.png")
    fig.write_image(f"{output_file}.pdf")
    fig.write_html(f"{output_file}.html")


# # Generate CSV files
# 

# In[7]:


for model in MODELS:
    generate_csv(model)


# # Load Information
# 

# In[8]:


dfs = {}
for model in MODELS:
    path = os.path.join(RESULTS_FOLDER, model, model)
    dfs[model] = read_csv(f"{path}.csv")
dfs[MODELS[0]]


# # Plots
# 

# In[9]:


for model in MODELS:
    for strategy in STRATEGIES:
        if model == "philo":
            df = dfs[model]
            df = df[df["synthesis"] == strategy]
            fig = plot(df, f"Synthesis (strategy={strategy})")
            save_plot(fig, model, f"results_{model}_{strategy}")
        else:
            for method in METHODS:
                df = dfs[model]
                df = df[(df["synthesis"] == strategy) & (df["method"] == method)]
                fig = plot(df, f"Synthesis (strategy={strategy}, method={method})")
                save_plot(fig, model, f"results_{model}_{strategy}_{method}")
fig


# # Tables
# 

# In[10]:


# Define a function to highlight the maximum value
def highlight_min(row, props):
    min_values = [row[s].min() for s in STRATEGIES]
    return [props if v in min_values else "" for v in row]


def highlight_cell(
    df, function, subset=STRATEGIES, latex=False, null_color="red", min_color="green"
):
    color_fmt = "color: {{{color}}}; bfseries: ;" if latex else "color: {color};"
    return (
        df.style.apply(
            function,
            axis=1,
            props=color_fmt.format(color=min_color),
            subset=subset,
        )
        .highlight_null(props=color_fmt.format(color=null_color))
        .format(na_rep="TO", precision=3)
    )


# In[11]:


def export_to_latex(
    df,
    model,
    function,
    filename,
    subset=STRATEGIES,
    highlight=True,
    null_color="BrickRed",
    min_color="OliveGreen",
):
    base_style = (
        highlight_cell(df, function, subset, True, null_color, min_color)
        if highlight
        else df.style
    )
    s = base_style.format_index("\\textbf{{{}}}", escape="latex", axis=1)

    return s.to_latex(
        os.path.join(
            create_folder(os.path.join(RESULTS_FOLDER, model)), f"{filename}.tex"
        ),
        hrules=True,
        clines="all;data",
    )


# In[12]:


for model in MODELS:
    columns = list(dfs[model].columns[:-3])
    table = dfs[model].pivot_table(
        "time", columns, ["synthesis", "method"], dropna=False
    )
    export_to_latex(table, model, highlight_min, f"times-{model}")
    table.to_markdown(os.path.join(RESULTS_FOLDER, model, f"time-{model}.md"))
highlight_cell(table, highlight_min)

